A local F-Droid repo, installed in `/system`

## Apps included

### Bitmask
- self built, see patch in `../apps/src/Bitmask.dif`f
### Briar
- https://f-droid.org/en/packages/org.briarproject.briar.android/
### Conversations
- https://f-droid.org/en/packages/eu.siacs.conversations/
### K-9 Mail
- https://f-droid.org/en/packages/org.openobservatory.ooniprobe/
### OONI Probe
- https://f-droid.org/en/packages/org.openobservatory.ooniprobe/
### OpenkeyChain
- https://f-droid.org/packages/org.sufficientlysecure.keychain/
### Orbot
- https://guardianproject.info/apps/orbot/
### Orfox - sunsetted by Tor Browser
### Signal
- https://signal.org/android/apk/
### Tor Browser
- https://www.torproject.org/download/download